﻿using EmployeeApp.Migrations;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMigrations(this IServiceCollection services, string connectionString)
        {
            return services
                .AddSingleton(provider => new EmployeeAppMigrationRunner(connectionString));
        }
    }
}