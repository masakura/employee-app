using EmployeeApp.Migrations;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.Hosting
{
    public static class HostExtensions
    {
        public static IHost MigrateUp(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var migrationRunner = scope.ServiceProvider.GetRequiredService<EmployeeAppMigrationRunner>();

                migrationRunner.MigrateUp();
            }

            return host;
        }
    }
}