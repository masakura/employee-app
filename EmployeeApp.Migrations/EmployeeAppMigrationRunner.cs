﻿using System;
using EmployeeApp.Migrations.Migrations;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;

namespace EmployeeApp.Migrations
{
    internal sealed class EmployeeAppMigrationRunner
    {
        private readonly string _connectionString;

        public EmployeeAppMigrationRunner(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void MigrateUp()
        {
            var migrationRunner = ConfigureServices().GetRequiredService<IMigrationRunner>();

            migrationRunner.MigrateUp();
        }

        private IServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddSQLite()
                    .WithGlobalConnectionString(_connectionString)
                    .ScanIn(typeof(AddEmployeeTable).Assembly).For.Migrations())
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .BuildServiceProvider();
        }
    }
}