﻿using FluentMigrator;

namespace EmployeeApp.Migrations.Migrations
{
    [Migration(201910151125)]
    public sealed class AddEmployeeTable : Migration
    {
        public override void Up()
        {
            Create.Table("employees")
                .WithColumn("id").AsGuid().PrimaryKey()
                .WithColumn("employeeNumber").AsInt32().Unique().NotNullable()
                .WithColumn("firstName").AsString(32).NotNullable()
                .WithColumn("lastName").AsString(32).NotNullable();
        }

        public override void Down()
        {
            Delete.Table("employees");
        }
    }
}