﻿using EmployeeApp.Application.Employees;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            return services
                .AddScoped<IEmployeeQueryService, EmployeeQueryService>()
                .AddScoped<IEmployeeAdditionalService, EmployeeAdditionalService>();
        }
    }
}