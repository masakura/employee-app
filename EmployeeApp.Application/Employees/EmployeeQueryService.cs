﻿using EmployeeApp.Domain.Models.Employees;

namespace EmployeeApp.Application.Employees
{
    internal sealed class EmployeeQueryService : IEmployeeQueryService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeQueryService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public EmployeeCollection All()
        {
            return _employeeRepository.All();
        }
    }
}