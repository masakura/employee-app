﻿using EmployeeApp.Domain.Models.Employees;

namespace EmployeeApp.Application.Employees
{
    sealed class EmployeeAdditionalService : IEmployeeAdditionalService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeAdditionalService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public EmployeeId Add(EmployeeProfile profile)
        {
            var id = EmployeeId.NewId();
            var employeeNumber = _employeeRepository.NewEmployeeNumber();

            var employee = new Employee(id, employeeNumber, profile);

            _employeeRepository.Add(employee);

            return id;
        }
    }
}