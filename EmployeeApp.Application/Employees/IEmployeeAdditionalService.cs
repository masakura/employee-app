﻿using EmployeeApp.Domain.Models.Employees;

namespace EmployeeApp.Application.Employees
{
    public interface IEmployeeAdditionalService
    {
        EmployeeId Add(EmployeeProfile profile);
    }
}