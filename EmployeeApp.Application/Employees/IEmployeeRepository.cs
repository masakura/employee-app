﻿using EmployeeApp.Domain.Models.Employees;

namespace EmployeeApp.Application.Employees
{
    public interface IEmployeeRepository
    {
        EmployeeCollection All();
        EmployeeNumber NewEmployeeNumber();
        void Add(Employee employee);
    }
}