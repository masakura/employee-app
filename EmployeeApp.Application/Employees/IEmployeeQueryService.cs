﻿using EmployeeApp.Domain.Models.Employees;

namespace EmployeeApp.Application.Employees
{
    /// <summary>
    ///     社員の取得。
    /// </summary>
    public interface IEmployeeQueryService
    {
        EmployeeCollection All();
    }
}