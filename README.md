# 増田本を読んで書いてみた
[増田本](https://gihyo.jp/book/2017/978-4-7741-9087-7)を読んで、実際に書いてみた。

三層 + ドメイン層がどうなっているのかを見せるため。ビジネスロジックがほとんどないので、アプリケーションサービスはほとんどスルーで DDD にしてうれしい! とはなっていない。


## セットアップ
事前に [.NET Core 3.0 SDK](https://dotnet.microsoft.com/download/dotnet-core/3.0) をインストールする。

```
git clone https://gitlab.com/masakura/employee-app.git
cd employee-app
cd EmployeeApp.WebApp
dotnet run
```

## 構造
プロジェクトの依存関係。

```plantuml
[Domain]
[Application]
[Infrastructure]
[WebApp]

[Application] --> [Domain]
[Infrastructure] --> [Domain]
[WebApp] --> [Domain]
[Infrastructure] --> [Application]
[WebApp] --> [Application]
[WebApp] ..> [Infrastructure]

note right of [WebApp] {
    プレゼンテーション層
    加えて、すべての層を統合する役目
}
```

ドメイン層はほかのどの層にも依存していないことがとても重要。

また、インフラストラクチャ層が実装すべきインターフェイスはアプリケーション層で宣言している。呼び出しは以下のようにアプリケーション層からインフラストラクチャ層。

```plantuml
[Application] --> [Infrastructure]
```

このままだと、インフラストラクチャ層を先に書くことになり、結果としてインフラストラクチャ層にごちゃごちゃ書かれてしまいがちになる。

これを避けるために、アプリケーション層を先に書き、アプリケーション層から見てほしいメソッドをインターフェイスとして定義する。その実装をインフラストラクチャ層に書く。

```plantuml
package application {
    [Application]
    interface "Repository Interface" as repository
}

[Infrastructure]

[Application] -> repository : use
[Infrastructure] -up-|> repository : implement
```


## 不明なところ
サービスクラスはアプリケーションサービスとドメインサービスがあるらしい。今回の例の、従業員番号を採番して従業員を登録するはビジネスロジックなので、ドメインサービスにあたるんだと思う。

この辺り区別つきにくいのもあるので、難しい。