﻿using System.Data;
using EmployeeApp.Application.Employees;
using EmployeeApp.Infrastructure.Employees;
using Microsoft.Data.Sqlite;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, string connectionString)
        {
            return services
                .AddScoped<IDbConnection>(provider => new SqliteConnection(connectionString))
                .AddScoped<IEmployeeRepository, EmployeeDataSource>();
        }
    }
}