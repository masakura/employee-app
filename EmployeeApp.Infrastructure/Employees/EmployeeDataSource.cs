﻿using System.Data;
using System.Linq;
using Dapper;
using EmployeeApp.Application.Employees;
using EmployeeApp.Domain.Models.Employees;

namespace EmployeeApp.Infrastructure.Employees
{
    internal sealed class EmployeeDataSource : IEmployeeRepository
    {
        private readonly IDbConnection _connection;

        public EmployeeDataSource(IDbConnection connection)
        {
            _connection = connection;
        }

        public EmployeeCollection All()
        {
            var employees = _connection.Query<Builder>("select * from employees;")
                .Select(builder => builder.Build());

            return new EmployeeCollection(employees);
        }

        public EmployeeNumber NewEmployeeNumber()
        {
            var result = _connection.ExecuteScalar("select max(employeeNumber) from employees;");

            if (result == null) return EmployeeNumber.LowestNumber();

            var maxEmployeeNumber = new EmployeeNumber((int) (long) result);
            return maxEmployeeNumber.Next();
        }

        public void Add(Employee employee)
        {
            var obj = new
            {
                Id = employee.Id.ToGuid(),
                EmployeeNumber = employee.EmployeeNumber.ToInt32(),
                FirstName = employee.Profile.Name.First.ToString(),
                LastName = employee.Profile.Name.Last.ToString()
            };

            _connection.Execute(@"
insert into employees values(@Id, @EmployeeNumber, @FirstName, @LastName);
", obj);
        }

        // ReSharper disable once ClassNeverInstantiated.Local
        private sealed class Builder
        {
            private readonly long _employeeNumber;
            private readonly string _firstName;
            private readonly string _id;
            private readonly string _lastName;

            public Builder(string id, long employeeNumber, string firstName, string lastName)
            {
                _id = id;
                _employeeNumber = employeeNumber;
                _firstName = firstName;
                _lastName = lastName;
            }

            public Employee Build()
            {
                return new Employee(new EmployeeId(_id),
                    new EmployeeNumber((int) _employeeNumber),
                    new EmployeeName(_firstName, _lastName));
            }
        }
    }
}