﻿namespace EmployeeApp.Domain.Models.Employees
{
    public sealed class EmployeeProfile
    {
        public EmployeeProfile(EmployeeName name)
        {
            Name = name;
        }

        public EmployeeName Name { get; }
    }
}