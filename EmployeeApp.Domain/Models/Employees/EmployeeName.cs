﻿using System;
using EmployeeApp.Domain.Models.Names;

namespace EmployeeApp.Domain.Models.Employees
{
    public sealed class EmployeeName : IEquatable<EmployeeName>
    {
        private readonly FullName _value;

        public EmployeeName(string first, string last) : this(new FullName(first, last))
        {
        }

        private EmployeeName(FullName value)
        {
            _value = value;
        }

        public FirstName First => _value.First;
        public LastName Last => _value.Last;

        public bool Equals(EmployeeName other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(_value, other._value);
        }

        public override string ToString()
        {
            return _value.ToString();
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is EmployeeName other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value != null ? _value.GetHashCode() : 0;
        }
    }
}