﻿using System;

namespace EmployeeApp.Domain.Models.Employees
{
    public sealed class EmployeeId : IEquatable<EmployeeId>
    {
        private readonly Guid _value;

        public EmployeeId(string value) : this(Guid.Parse(value))
        {
        }

        public EmployeeId(Guid value)
        {
            _value = value;
        }

        public bool Equals(EmployeeId other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _value.Equals(other._value);
        }

        public Guid ToGuid()
        {
            return _value;
        }

        public override string ToString()
        {
            return _value.ToString();
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is EmployeeId other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public static EmployeeId NewId()
        {
            return new EmployeeId(Guid.NewGuid());
        }
    }
}