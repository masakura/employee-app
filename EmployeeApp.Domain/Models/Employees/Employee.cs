﻿namespace EmployeeApp.Domain.Models.Employees
{
    public sealed class Employee
    {
        public Employee(EmployeeId id, EmployeeNumber employeeNumber, EmployeeName name) :
            this(id, employeeNumber, new EmployeeProfile(name))
        {
        }

        public Employee(EmployeeId id, EmployeeNumber employeeNumber, EmployeeProfile profile)
        {
            Id = id;
            EmployeeNumber = employeeNumber;
            Profile = profile;
        }

        public EmployeeId Id { get; }
        public EmployeeNumber EmployeeNumber { get; }
        public EmployeeProfile Profile { get; }
    }
}