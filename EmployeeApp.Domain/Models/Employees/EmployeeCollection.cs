﻿using System.Collections.Generic;
using System.Linq;

namespace EmployeeApp.Domain.Models.Employees
{
    public sealed class EmployeeCollection
    {
        private readonly IEnumerable<Employee> _items;

        public EmployeeCollection(IEnumerable<Employee> items)
        {
            _items = items.ToArray();
        }

        public IEnumerable<Employee> AsEnumerable()
        {
            return _items.AsEnumerable();
        }
    }
}