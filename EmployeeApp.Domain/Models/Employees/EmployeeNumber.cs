﻿using System;

namespace EmployeeApp.Domain.Models.Employees
{
    public sealed class EmployeeNumber : IEquatable<EmployeeNumber>
    {
        private readonly int _value;

        public EmployeeNumber(int value)
        {
            if (value <= 0 || value > 99999) throw new InvalidOperationException();

            _value = value;
        }

        public bool Equals(EmployeeNumber other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _value == other._value;
        }

        public override string ToString()
        {
            return $"{_value:D5}";
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is EmployeeNumber other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value;
        }

        public EmployeeNumber Next()
        {
            return new EmployeeNumber(_value + 1);
        }

        public int ToInt32()
        {
            return _value;
        }

        public static EmployeeNumber LowestNumber()
        {
            return new EmployeeNumber(1);
        }
    }
}