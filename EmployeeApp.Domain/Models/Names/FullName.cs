﻿using System;

namespace EmployeeApp.Domain.Models.Names
{
    public sealed class FullName : IEquatable<FullName>
    {
        public FullName(string first, string last) : this(new FirstName(first), new LastName(last))
        {
        }

        private FullName(FirstName first, LastName last)
        {
            First = first;
            Last = last;
        }

        public FirstName First { get; }
        public LastName Last { get; }

        public override string ToString()
        {
            return $"{Last} {First}";
        }

        public bool Equals(FullName other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(First, other.First) && Equals(Last, other.Last);
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is FullName other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((First != null ? First.GetHashCode() : 0) * 397) ^ (Last != null ? Last.GetHashCode() : 0);
            }
        }
    }
}