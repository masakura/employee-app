﻿using System;

namespace EmployeeApp.Domain.Models.Names
{
    public sealed class LastName : IEquatable<LastName>
    {
        private readonly string _value;

        public LastName(string value)
        {
            _value = value;
        }

        public bool Equals(LastName other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _value == other._value;
        }

        public override string ToString()
        {
            return _value;
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is LastName other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value != null ? _value.GetHashCode() : 0;
        }
    }
}