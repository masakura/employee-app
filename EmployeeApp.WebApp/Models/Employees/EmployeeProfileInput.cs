﻿using EmployeeApp.Domain.Models.Employees;

namespace EmployeeApp.WebApp.Models.Employees
{
    public sealed class EmployeeProfileInput
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public EmployeeProfile ToProfile()
        {
            return new EmployeeProfile(new EmployeeName(FirstName, LastName));
        }
    }
}