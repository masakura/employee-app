﻿using EmployeeApp.Application.Employees;
using EmployeeApp.WebApp.Models.Employees;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeApp.WebApp.Controllers
{
    public sealed class EmployeesController : Controller
    {
        private readonly IEmployeeAdditionalService _employeeAdditionalService;
        private readonly IEmployeeQueryService _employeeQueryService;

        public EmployeesController(IEmployeeQueryService employeeQueryService,
            IEmployeeAdditionalService employeeAdditionalService)
        {
            _employeeQueryService = employeeQueryService;
            _employeeAdditionalService = employeeAdditionalService;
        }

        public IActionResult Index()
        {
            var employees = _employeeQueryService.All();

            return View(employees);
        }

        public IActionResult Add()
        {
            return View(new EmployeeProfileInput());
        }

        [HttpPost]
        public IActionResult Add(EmployeeProfileInput input)
        {
            var profile = input.ToProfile();

            _employeeAdditionalService.Add(profile);

            return RedirectToAction(nameof(Index));
        }
    }
}